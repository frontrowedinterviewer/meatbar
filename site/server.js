const csvToJson = require('csvtojson')
const express = require('express')
const path = require('path');
const request = require('request');

const app = express()

app.use(express.static(path.resolve(__dirname)));

app.get('/', function(req, res) {
  res.sendFile(path.resolve(__dirname, 'index.html'));
});

app.get('/consumptions', function(req, res) {
  const csvFilePath = path.resolve(__dirname, 'data.csv');
  const objs = [];
  csvToJson()
    .fromFile(csvFilePath)
    .on('json',(jsonObj)=>{
      objs.push(jsonObj);
    })
    .on('done',(error)=>{
      res.json(objs);
      console.log('end')
    })
});

app.listen(3000);
