function drawChart(dataRows) {
  // Create the data table.
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Meat Type');
  data.addColumn('number', 'Number Consumed');
  const rows = [];
  for (const meat of Object.keys(dataRows.meats)) {
    rows.push([meat, dataRows.meats[meat]]);
  }
  data.addRows(rows);

  // Set chart options
  var options = {
  'legend':'left',
  'width':500,
  'height':500};

  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.PieChart(document.getElementsByClassName('consumptionChart')[0]);
  chart.draw(data, options);
}

const RowItem = React.createClass({
    propTypes: {
      name: React.PropTypes.string,
      consumptionCount: React.PropTypes.number,
      clickFunc: React.PropTypes.func,
      isSelected: React.PropTypes.bool
    },

    render() {
      return (
        <tr onClick={() => this.props.clickFunc(this.props.name)} className={this.props.isSelected ? 'selected' : ''}>
          <td>
            {this.props.name}
          </td>
          <td>
            {this.props.consumptionCount}
          </td>
        </tr>
      );
    }
});

const Consumption = React.createClass({
  propTypes: {
    selectedData: React.PropTypes.object,
    isChartReady: React.PropTypes.bool
  },

  componentWillReceiveProps(nextProps) {
    if (nextProps.isChartReady) {
      drawChart(this.props.selectedData);
    }
  },

  renderHeader() {
    if (!this.props.selectedData) {
      return null;
    }
    return (
      <h4>Meat consumed by {this.props.selectedData.name}</h4>
    );
  },

  renderData() {
    return (
      <div className='consumptionChart'>
      </div>
    )
  },

  render() {
    return (
      <div className='consumption content-piece col-md-6'>
        {this.renderHeader()}
        {this.renderData()}
      </div>
    )
  }
});

const Customers = React.createClass({
  propTypes: {
    data: React.PropTypes.object,
    selectedData: React.PropTypes.object,
    onSelect: React.PropTypes.func
  },

  clickFunc(name) {
    this.props.onSelect(this.props.data[name]);
  },

  renderHeader() {
    return (
      <h4>Number of consumptions per consumer</h4>
    );
  },

  renderSubheader() {
    return (
      <thead>
        <tr>
          <th>Name</th>
          <th>Consumptions</th>
        </tr>
      </thead>
    );
  },

  renderData() {
    return Object.keys(this.props.data).map(person => {
      return (
        <RowItem
          isSelected={this.props.data[person]===this.props.selectedData}
          key={person} clickFunc={this.clickFunc}
          name={person}
          consumptionCount={this.props.data[person].total}
        />
      );
    });
  },

  render() {
    if (!this.props.data) {
      return null;
    }
    return (
      <div className='customers content-piece col-md-6'>
        {this.renderHeader()}
        <table className='table table-hover'>
          {this.renderSubheader()}
          <tbody>
            {this.renderData()}
          </tbody>
        </table>
      </div>
    )
  }
});

function aggregateData(data) {
  const aggregatedData = {};
  data.map((entity) => {
    if (!(entity.person in aggregatedData)) {
      aggregatedData[entity.person] = {total: 0, name: entity.person, meats: {}}
    }
    if (!(entity['meat-bar-type'] in aggregatedData[entity.person].meats)) {
      aggregatedData[entity.person].meats[entity['meat-bar-type']] = 0;
    }
    aggregatedData[entity.person].meats[entity['meat-bar-type']] += 1;
    aggregatedData[entity.person].total += 1;
  });
  return aggregatedData;
}

const App = React.createClass({
  getInitialState() {
    return {
      store: null,
      selected: null,
      isChartReady: false
    };
  },

  componentWillMount() {
    $.get('/consumptions').done((result) => {
      const aggregatedData = aggregateData(result);
      this.setState({
        store: aggregatedData,
        selected: aggregatedData[Object.keys(aggregatedData)[0]]
      });
    });
  },

  componentDidMount() {
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(() => this.setState({isChartReady: true}));
  },

  setSelected(newSelected) {
    this.setState({
      selected: newSelected
    });
  },

  renderTitle() {
    return (
      <div className='title'>
        <h2>Meat</h2>
        <h4>data analysis for Epic Meat Bars</h4>
      </div>
    );
  },

  renderContent() {
    return (
      <div className='content row'>
        <Customers selectedData={this.state.selected} onSelect={this.setSelected} data={this.state.store} />
        <Consumption isChartReady={this.state.isChartReady} selectedData={this.state.selected} />
      </div>
    );
  },

  render() {
    return (
      <div className='container'>
        {this.renderTitle()}
        {this.renderContent()}
      </div>
    );
  }
});

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
